from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException, ElementNotSelectableException
from selenium.webdriver.support.wait import WebDriverWait
from appium.webdriver.common.mobileby import MobileBy
import utilities.CustomLogger as cl
import time
from src.testproject.decorator import report


class BasePage:
    log = cl.customLogger()

    def __init__(self, driver):
        self.driver = driver

    def waitForElement(self, locatorValue, locatorType):
        element = None
        wait = WebDriverWait(self.driver, 25, poll_frequency=1,
                             ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException,
                                                 NoSuchElementException])

        if locatorType == "id":
            element = wait.until(lambda x: x.find_element_by_id(locatorValue))
            return element
        elif locatorType == "class":
            element = wait.until(lambda x: x.find_element_by_class_name(locatorValue))
            return element
        elif locatorType == "des":
            element = wait.until(
                lambda x: x.find_element_by_android_uiautomator('UiSelector().description("%s")' % locatorValue))
            return element
        elif locatorType == "index":
            element = wait.until(
                lambda x: x.find_element_by_android_uiautomator("UiSelector().index(%d)" % int(locatorValue)))
            return element
        elif locatorType == "text":
            element = wait.until(lambda x: x.find_element_by_android_uiautomator('text("%s")' % locatorValue))
            return element
        elif locatorType == "XPATH":
            element = wait.until(lambda x: x.find_element_by_xpath('%s' % locatorValue))
            return element
        elif locatorType == "MobileBy.ACCESSIBILITY_ID":
            element = wait.until(lambda x: x.find_element(MobileBy.ACCESSIBILITY_ID, '%s' % locatorValue))
            return element
        elif locatorType == "MobileBy.IOS_PREDICATE_TEXT":
            element = wait.until(lambda x: x.find_element(MobileBy.IOS_PREDICATE, '%s' % locatorValue))
            return element
        elif locatorType == "MobileBy.IOS_PREDICATE_SECURE_FIELD":
            element = wait.until(lambda x: x.find_element(MobileBy.IOS_PREDICATE, '%s' % locatorValue))
            return element
        else:
            self.log.info("Locator value " + locatorValue + "not found")

        return element

    def getElement(self, locatorValue, locatorType):
        element = None
        try:
            element = self.waitForElement(locatorValue, locatorType)
            self.log.info("Element found with LocatorType: " + locatorType + " with the locatorValue :" + locatorValue)
        except:
            self.log.info(
                "Element not found with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue)

        return element

    def clickElement(self, locatorValue, locatorType):
        try:
            element = self.getElement(locatorValue, locatorType)
            element.click()
            self.log.info(
                "Clicked on Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue)
        except:
            self.log.info(
                "Unable to click on Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue)

    def sendText(self, text, locatorValue, locatorType):
        try:
            element = self.getElement(locatorValue, locatorType)
            element.click()
            element.send_keys(text)
            self.log.info(
                "Send text  on Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue)
        except:
            self.log.info(
                "Unable to send text on Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue)

    def isDisplayed(self, locatorValue, locatorType):
        try:
            element = self.getElement(locatorValue, locatorType)
            element.is_displayed()
            self.log.info(
                " Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue + " is displayed ")
            return True
        except:
            self.log.info(
                " Element with LocatorType: " + locatorType + " and with the locatorValue :" + locatorValue + " is not displayed")
            return False

    def hideKeyboard(self):
        self.driver.hide_keyboard()

    def screenShot(self, screenshotName):
        fileName = screenshotName + "_" + (time.strftime("%d_%m_%y_%H_%M_%S")) + ".png"
        screenshotDirectory = "../screenshots/"
        screenshotPath = screenshotDirectory + fileName
        try:
            self.driver.report().step(
                description='Page: Search results',
                message='Taking screenshot',
                passed=True,
                screenshot=True
            )
            self.driver.save_screenshot(screenshotPath)
            self.log.info("Screenshot save to Path : " + screenshotPath)

        except:
            self.log.info("Unable to save Screenshot to the Path : " + screenshotPath)

    def swipeDown(self):
        self.driver.execute_script("mobile: scroll", {"direction": "down"})

