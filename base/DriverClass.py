from src.testproject.sdk.drivers import webdriver


class Driver:

    def getDriverMethod(self):
        capabilities = {
            "platformName": "iOS",
            "udid": "1e9e200906ecdd56bea1eff52adecd8c143fc058",
            "bundleId": "com.circadn.coachfirsthubdev",
        }

        driver = webdriver.Remote(token="DDCwQL2zHPRnD2Lsx8s1-cpNruGwiuFwYAhz3OpCS2E1", project_name="CoachFirstAutomation", job_name="TestLogin", desired_capabilities=capabilities)

        return driver


