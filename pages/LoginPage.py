from base.BasePage import BasePage


class LoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Locator values in Login Page
    _loginButton = "LOG IN"
    _anotherLoginButton = "LOG IN WITH OTHER ACCOUNT"
    _emailAddress = "type == 'XCUIElementTypeTextField' AND visible == 1"
    _password = "type == 'XCUIElementTypeSecureTextField' AND visible == 1"
    _moreSettings = "More, tab, 5 of 5"
    _signOut = "Sign Out"
    _signoutYes = "Yes"

    def verifyAppOpen(self):
        element = self.isDisplayed(self._loginButton, "MobileBy.ACCESSIBILITY_ID")
        assert element == True

    def clickLoginButton(self):
        self.clickElement(self._loginButton, "MobileBy.ACCESSIBILITY_ID")
        self.clickElement(self._anotherLoginButton, "MobileBy.ACCESSIBILITY_ID")

    def enterEmail(self):
        self.sendText("pranabin.ghosh@yopmail.com", self._emailAddress, "MobileBy.IOS_PREDICATE_TEXT")

    def enterPassword(self):
        self.sendText("Test@1234", self._password, "MobileBy.IOS_PREDICATE_SECURE_FIELD")

    def finalLoginSubmit(self):
        self.hideKeyboard()

    def clickMore(self):
        self.clickElement(self._moreSettings, "MobileBy.ACCESSIBILITY_ID")

    def swipetoDown(self):
        self.swipeDown()

    def clickSignout(self):
        self.clickElement(self._signOut, "MobileBy.ACCESSIBILITY_ID")

    def acceptSignout(self):
        self.clickElement(self._signoutYes, "MobileBy.ACCESSIBILITY_ID")

    def logintoDashboard(self, email_address, password):
        self.clickElement(self._loginButton, "MobileBy.ACCESSIBILITY_ID")
        self.clickElement(self._anotherLoginButton, "MobileBy.ACCESSIBILITY_ID")
        self.sendText(email_address, self._emailAddress, "MobileBy.IOS_PREDICATE_TEXT")
        self.sendText(password, self._password, "MobileBy.IOS_PREDICATE_SECURE_FIELD")
        self.hideKeyboard()

